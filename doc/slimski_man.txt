slimski(1)                                                                                          slimski(1)

NAME
       slimski - graphical Login Manager for X11

SYNOPSIS
       slimski [options] [<arguments>]

DESCRIPTION
       slimski is a  desktop-independent graphical login manager for X11,
       enabling the initialization of a graphical session by entering username and
       password in a login screen. slimski is configurable via themes and via an options
       (conf) file. It is suitable for machines that do not require remote logins.

OPTIONS
       -d     run as a daemon
                When this option is not specified, user is returned to shell prompt
                upon graphical session logout. Declaring this option may unnecessary
                when slimski is started via a runlevel initscript
                (see: /etc/init.d/debian/slimski)

       -p <name_of_theme>  (example:  slimski -p twilite )
              display a preview of the theme. An already running X11 session is required
              for theme preview. Preview mode is useful (necessary!) when creating/editing
              themes, in order to tweak the x,y placement of displayed message strings.
              It also enables you to interactively test the message which would be displayed
              in the event of an invalid login attempt. Upon successful test ´login´,
              slimski will display a welcome_msg for 3 seconds then will exit.

       When used in tandem with other options, "-p" (and its argument) should be placed last.
       note: prior to using preview, you should read Localization manpage section, below

       -s     (only recognized in tandem with -p)
                showall config and theme variable::value pairs

       -z     (recommended for use only in tandem with -p)
                displays verbose runtime messages to stdout
                (paths//files read, non-fatal error messages, etc.)
                Useful for troubleshooting misconfigured config+theme options

       -i     ignore env $LANG, load slimski.theme without checking localized variant.
              Instead of using this option you can, alternatively, export LANG=<xx__YY>
              to change the runtime environment LANG between successive theme tests.

       -h     display a brief help message

       -v     display version information

EXAMPLES
       slimski -d
              run slimski in daemon mode

       slimski -p /usr/share/slimski/themes/twilite
              display a preview of the twilite theme

STARTING slimski AT BOOT
       (see: /etc/init.d/debian/slimski
        which is autoinstalled during debfile installation)
       For non-debian -based installations, please refer to the documentation of your
       Operating System on how to set automatic post-boot slimski startup. Typically,
       slimski can be loaded on startup by declaring it within your daemons array
       in rc.conf, or by modifying inittab

CONFIGURATION
       Global configuration is stored in the /etc/slimski.conf file. Refer to the
       inline comments within the file for a detailed explanation of the configurable
       options. Also, you can refer to /usr/share/doc/slimski/slimski_OPTIONS_LIST.txt
       for a comprehensive list of options, along with descriptions and examples.

THEMING
       slimski theming documentation is provided in the file
        /usr/share/doc/slimski/THEMES

       Resource files for slimski themes reside in subdirectories of
        /usr/share/slimski/themes

       Localization: slimski consults the LANG runtime environmental variable, and will
       check for the existence of a matching localized __nn_NN variant of the
       specified theme.

       For example: At runtime, if slimski detects LANG=en_GB.UTF-8
       and the themename specified in /etc/slim.conf is twilite
       it would load
            /usr/share/slimski/themes/twilite/slimski__en_GB.theme
       if present. Otherwise it would fallback to loading
            /usr/share/slimski/themes/twilite/slimski.theme

       Currently (Jan 2021), you should expect to find very few localized theme variants
       (or none) included as pre-installed items within the installation package.
       The onus of creating localized variants rests with distro maintainers and/or
       local sysadmins.

       Additional theming details are outlined in the file
        /usr/share/doc/slimski/THEMING

USAGE AND SPECIAL USERNAMES
       When started, slimski will show a login panel; enter the username and password
       of the user you want to login as.

       Special usernames
       (when typed into the username input field, these execute actions other than login):

       exit   quit slimski; exit (to command prompt)
              (configurable, disabled by default ~~~ sysadmin can elect to enable)

       halt   shutdown the machine (requires root* password)
              (root, or other user account with suitable permissions)

       reboot reboot the machine    (requires root* password)

       Note: The local sysadmin, via slimski.conf, may specify enabled or disabled for
             each of these commands

       slimski performs a PAM (Pluggable Authentication module) authentication check when
       a  halt (aka systemhalt) | reboot | exit command is attempted via special username entry.

       See the configuration file for customizing the above commands. Regardless of external
       configuration, slimski will, unconditionally, demand the root password prior to
       initiating a 'halt' (aka systemhalt) or 'reboot' command.

       Note regarding "exit": Bear in mind that a system's runlevel configuration may
       enforce an immediate restart of slimski.

Available Shortcuts (keybinds)
       F1     choose sessiontype (e.g rox+icewm, fluxbox, min-jwm, startxfce4)
              This keybind is available (and a textlabel advertises its availability) when
              multiple types have been specified within slimski.conf

       F11    Pressing the F11 key executes a CUSTOM COMMAND (as configured within the
              /etc/slimski.conf configuration file). Traditionally, under SLiM, F11 assignment
              had often specified "scrot /root/slimski.png" (take a screenshot of the
              login screen). For slimski, the as-shipped default F11 configuration is blank.

       F5     slimski can be configured to execute an additional CUSTOM COMMAND when F5
              keypress is detected during login screen display. You might utilize F5 to
              launch a windowed program (yad, zenity, xdialog) to display helptext. Bear in
              mind that slimski does not know, does not check, whether or not the program you
              have specified is installed on the system. It is advisable to enable mouse cursor
              and choose a program whose window will/can launch in fullscreen display mode.
              Otherwise, a  click outside the bounds of the window can cause it to wind up
              lost from view, obscured by the fullscreen slimski background. (If this occurs,
              upon successful login the user would discover the still-running program displaying
              its window and could then click to close its window.)

       Security note regarding custom commands:
              When choosing a command or program for F5/F11 assignment, bear in mind that
              if the slimski login manager program was launched with elevated permissions
              (which is the case if debian xserver-xorg-legacy is installed on your
              system ~~ it dictates that only root may start the X server), any program launched
              via F5/F11(or other custom-configured F2--F12 key)  would also launch with
              elevated permissions.
                 -=-
              Even if slimski was NOT launched with elevated permissions, it would be unwise
              (insecure) to run a command which is capable of writing an output file to /tmp/.
              Anyone with local access to the system might place a symlink there (at the known
              destination), a symlink pointing to a critical system file such as /usr/sbin/init
                 -=-
              The "even if" paragraph, above, is paraphrased from a warning included in the
              debian source package for SLiM. An expanded warning applies, regarding the
              possible assignment of Fkeys being configured to launch an interactive program.
              Even a seemingly innocuous "text editor" program, via a  "SaveAs" operation,
              could be utilized by an attacker to (e.g.) overwrite critical system files.

TIPS
       (The following is applicable to operating systems other than antiX)
       If you have trouble starting your desktop environment: Understand that your
       ~/.xinitrc file is executed by default; ensure a working .xinitrc file exists
       in your home directory. An example ~/.xinitrc file is included with the slimski
       source code, and slimski debfile package installation places a copy at
       /usr/share/doc/slimski/xinitrc.sample

FILES
       Upon successful login, ~/.Xauthority is written, bearing the datetime of login

       /var/log/slimski.log
       /etc/init.d/slimski  or (YMMV) /etc/init.d/debian/slimski
       /etc/pam.d/slimski
       /etc/X11/Xsession.d/20slimski_locale
       /etc/apt/apt.conf.d/99-update-slim (as packaged for debian and derivatives)
       /etc/slimski.conf
       /etc/pam.d/slimski
       /usr/bin/slimski
       /usr/share/doc/slimski/{various}
       /usr/share/slimski/themes/{various}
       (per user) ~/.config/slimski.lastused
       (as packaged for debian, see also:
         conf.slimski_orig and xinitrc.sample, installed to /usr/share/doc/slimski directory)

PROJECT PAGE
       project source code repository: https://gitlab.com/skidoo/slimski

AUTHORS
       slimski is derived from SLiM (Simple LogIn Manager), which was originally authored by:

       Simone Rota <sip@varlock.com>

       Johannes Winkelmann <jw@tks6.net> Nobuhiro Iwamatsu <iwamatsu@nigauri.org>

                                               January 01, 2021                                     slimski(1)
