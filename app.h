/* slimski - Simple Login Manager
   Copyright (C) 1997, 1998 Per Liden
   Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
   Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
*/

#ifndef _APP_H_
#define _APP_H_

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <setjmp.h>
#include <stdlib.h>
#include <iostream>
#include "panel.h"
#include "cfg.h"
#include "image.h"
#include "PAM.h"
#include "pwd.h"     //   getpwnam   (getspnam which we do not invoke, would also require shadow.h)

extern bool testing;
extern bool zeebug;

class App {
public:
    App(int argc, char** argv);
    ~App();
    void Run();
    void RestartServer();
    void StopServer();

    void GetPIDLock();
    void RemovePIDLock();

    bool isServerStarted();

private:
    void Login();     // disambiguated fn name   (tried   DoLogin()    then reverted)
    void Reboot();
    void Halt();
    void Exit();
    void Suspend();
    void KillAllClients(Bool top);
    void OpenLog();
    void CloseLog();
    void HideMouseCursor();
    void CreateServerAuth();
    char* StrConcat(const char* str1, const char* str2);
    unsigned long GetColor(const char* colorname);
    XftColor backgroundcolor;
    XftColor smackgroundcolor;
    void UpdatePid();

    bool AuthenticateUser(bool focuspass);

    static void replaceVariables(std::string& input, const std::string& var, const std::string& value);

    // Server functions
    int StartServer();
    int ServerTimeout(int timeout, char *string);
    int WaitForServer();

    // Private data
    Window Rootwin;
    Window RealWoot;
    Display* Dpy;
    int Scr;

    Panel* LoginPanel;
    int ServerPID;
    const char* DisplayName;
    bool serverStarted;

    PAM::Authenticator pam;

    // Options
    char* DispName;

    Cfg *cfg;

    Pixmap BackgroundPixmap;
    ////Pixmap LogoPixmap;         //  howdy      might later rename and repurpose these

    void blankScreen();
    Image* bimage;
    ////Image* logoimage;
    Atom BackgroundPixmapId;
    void setBackground(const std::string& themedir);

    bool firstlogin;
    bool daemonmode;
    bool force_nodaemon;

    ////bool showlogo;
    bool showallvarsvals;
    bool ignorelang;
    bool wants_popfirst;

    std::string testtheme;    //    howdy    WAS  char*
    std::string themeName;
    std::string mcookie;

    const int mcookiesize;
};


#endif
